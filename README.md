# Kali SinuS ISO image

##### *forked from [Kali Linux/build-scripts/live-build-config](https://gitlab.com/kalilinux/build-scripts/live-build-config)*

## Customized live-build configuration for building Kali SinuS ISO image

This is the customized live-build configuration for building what I call the **Kali SinuS ISO Image**. It has a modified Gnome desktop environment and because of the smaller amount of packages included it's a quite lightweight variant. For convenience I have also included a few additional [source repositories](https://github.com/sinusphi/live-build-config/tree/master/kali-config/variant-sinus/includes.chroot/etc/apt/sources.list.d) to APT. However, most of it are disabled by default, if you want to enable any, just comment out the lines you need.

The appearance is customized in different ways, for details have a look at the following directories.

For [hooks](https://gitlab.com/sinusphi1/live-build-config#build-hooks-binary-and-chroot) executed in the chroot after the rest of the chroot configuration has been applied:
```bash
live-build-config/kali-config/variant-sinus/hooks/
```

For the [list](https://gitlab.com/sinusphi1/live-build-config#controlling-the-packages-included-in-your-build) of packages included in the build:
```bash
live-build-config/kali-config/variant-sinus/packages-lists/
```

For [additional files or scripts included](https://gitlab.com/sinusphi1/live-build-config#overlaying-files-in-your-build) in the build:
```bash
live-build-config/kali-config/variant-sinus/includes.chroot/
```

### Some of the preinstalled apps

- [Firefox Developer Edition](https://www.mozilla.org/en-US/firefox/developer/)
- [Visual Studio Code](https://code.visualstudio.com/)
- [IPython](https://ipython.org/)
- [Remarkable](https://remarkableapp.github.io/linux.html)
- [Libre Office](https://www.libreoffice.org/)
- [VLC Media Player](https://www.videolan.org/)
- [Arc Theme](https://www.gnome-look.org/p/1181106/)
- [Hardware Lister (lshw)](https://ezix.org/project/wiki/HardwareLiSter)
- [Cheese](https://wiki.gnome.org/Apps/Cheese)
- [Simple Scan](https://gitlab.gnome.org/GNOME/simple-scan)
- [Tweaks](https://wiki.gnome.org/Apps/Tweaks)

### Further reading

Instructions for building a Kali Linux ISO for older i386 architectures, and for building Kali on Non-Kali Debian Based Systems can be found at https://docs.kali.org/development/live-build-a-custom-kali-iso. More about customizing Kali at https://docs.kali.org/kali-dojo/02-mastering-live-build.

___

## From [Kali Docs](https://docs.kali.org/development/live-build-a-custom-kali-iso):

### Where Should You Build Your ISO?

Ideally, you should build your custom Kali ISO from within a **pre-existing Kali environment**.

### Getting Ready - Setting up the live-build system

We first need to prepare the Kali ISO build environment by installing and setting up live-build and its requirements with the following commands:
```bash
$ apt install -y curl git live-build cdebootstrap
$ git clone https://gitlab.com/kalilinux/build-scripts/live-build-config.git
```

Now you can simply build an updated Kali ISO by entering the `live-build-config` directory and running our `build.sh` wrapper script, as follows:
```bash
$ cd live-build-config/
$ ./build.sh --verbose
```

The `build.sh` script will take a while to complete, as it downloads all of the required packages needed to create your ISO. Good time for a coffee.
___

## Configuring the Kali ISO Build (Optional)

If you want to customize your Kali Linux ISO, this section will explain some of the details. Through the kali-config directory, the Kali Linux live build supports a wide range of customization options, which are well-documented on the Debian live build 4.x page. However, for the impatient, here are some of the highlights.

### Building Kali with Different Desktop Environments

Since Kali 2.0, we now support built in configurations for various desktop environments, including KDE, Gnome, E17, I3WM, LXDE, MATE and XFCE. To build any of these, you would use syntax similar to the following:

```bash
# These are the different Desktop Environment build options:
#./build.sh --variant {gnome,kde,xfce,mate,e17,lxde,i3wm} --verbose

# To build a KDE ISO:
./build.sh --variant kde --verbose
# To build a MATE ISO:
./build.sh --variant mate --verbose

#...and so on.
```

### Controlling the packages included in your build

The list of packages included in your build will be present in the the respective `kali-$variant` directory. For example, if you're building a default Gnome ISO, you would use the following package lists file - `kali-config/variant-gnome/package-lists/kali.list.chroot`. By default, this list includes the `kali-linux-full` metapackage, as well as some others. These can be commented out and replaced with a manual list of packages to include in the ISO for greater granularity.

### Build hooks, binary and chroot

Live-build hooks allows us to hook scripts in various stages of the Kali ISO live build. For more detailed information about hooks and how to use them, refer to the live build manual. As an example, we recommend you check out the existing hooks in `kali-config/common/hooks/`.

### Overlaying files in your build

You have the option to include additional files or scripts in your build by overlaying them on the existing filesystem, inside the includes.{chroot,binary,installer} directories, respectively. For example, if we wanted to include our own custom script into the `/root/` directory of the ISO (this would correspond to the `chroot` stage), then we would drop this script file in the `kali-config/common/includes.chroot/` directory before building the ISO.
