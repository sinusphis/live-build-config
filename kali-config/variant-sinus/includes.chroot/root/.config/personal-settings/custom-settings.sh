#!/bin/sh


# set applications shown in Dash-to-Dock
gsettings set org.gnome.shell favorite-apps "['org.gnome.Terminal.desktop', 'gnome-system-monitor.desktop', 'gnome-control-center.desktop', 'org.gnome.tweaks.desktop', 'code.desktop', 'ipython.desktop', 'org.gnome.Nautilus.desktop', 'vlc.desktop', 'org.gnome.gedit.desktop', 'firefox-developer.desktop']"

# activate gnome-shell extensions
gsettings set org.gnome.shell enabled-extensions "['apps-menu@gnome-shell-extensions.gcampax.github.com', 'places-menu@gnome-shell-extensions.gcampax.github.com', 'workspace-indicator@gnome-shell-extensions.gcampax.github.com', 'dash-to-dock@micxgx.gmail.com', 'user-theme@gnome-shell-extensions.gcampax.github.com', 'jasonmun@yahoo.com', 'harddiskled@bijidroid.gmail.com', 'audio-switcher@AndresCidoncha', 'github.notifications@alexandre.dufournet.gmail.com', 'modern-calc@kaer']"

# set region to germany
dconf write /org/system/locale/region "de_DE.UTF-8"

# customize desktop
gsettings set org.gnome.desktop.background picture-uri file:///root/.config/personal-settings/kali-red.jpg
gsettings set org.gnome.desktop.screensaver picture-uri file:///root/.config/personal-settings/kali-red.jpg
gsettings set org.gnome.desktop.interface gtk-theme "Arc-Darker"
gsettings set org.gnome.desktop.interface icon-theme "Zen-Kali-Dark"
gsettings set org.gnome.desktop.wm.preferences theme "Arc-Dark"
gsettings set org.gnome.desktop.app-folders folder-children "[]"

# customize dash-to-dock
dconf write /org/gnome/shell/extensions/dash-to-dock/autohide true
dconf write /org/gnome/shell/extensions/dash-to-dock/dock-fixed false
dconf write /org/gnome/shell/extensions/dash-to-dock/running-indicator-style "'DOTS'"
dconf write /org/gnome/shell/extensions/dash-to-dock/transparency-mode "'DYNAMIC'"
dconf write /org/gnome/shell/extensions/dash-to-dock/scroll-switch-workspace false

# load terminal settings
dconf load /org/gnome/terminal/ < /root/.config/personal-settings/terminal-preferences

# customize Auto OVPN
dconf write /org/gnome/shell/extensions/auto-ovpn/position-in-panel "'center'"
dconf write /org/gnome/shell/extensions/auto-ovpn/icon-size "'16'"
dconf write /org/gnome/shell/extensions/auto-ovpn/refresh-rate "299"

# customize clock
dconf write /org/gnome/desktop/interface/show-battery-percentage "true"
dconf write /org/gnome/desktop/interface/clock-show-date "true"
dconf write /org/gnome/desktop/sound/allow-volume-above-100-percent "true"

# customize gedit
dconf write /org/gnome/gedit/preferences/editor/scheme "'cobalt'"
dconf write /org/gnome/gedit/preferences/editor/display-line-numbers "true"
dconf write /org/gnome/gedit/preferences/editor/highlight-current-line "true"
dconf write /org/gnome/gedit/preferences/editor/insert-spaces "true"
dconf write /org/gnome/gedit/preferences/editor/tabs-size "uint32 4"

#]=============================================================================[#

# patch vlc binary (ability to run vlc as root)
python3 /root/.config/personal-settings/vlc-patch.py

