#!/bin/sh


if [ -e /root/.config/personal-settings/custom-settings.sh ]
then
    /root/.config/personal-settings/custom-settings.sh
    mv /root/.config/personal-settings/custom-settings.sh /root/.config/personal-settings/custom-settings.sh.backup
fi
