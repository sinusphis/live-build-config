#!/usr/bin/env python3
from time import sleep


target = "/usr/bin/vlc"
geteuid = b"geteuid"
getppid = b"getppid"


try:
    with open(target, "rb") as binary:
        bn = binary.name
        print(f"Verifying file '{bn}'...")
        content = binary.read()
        sleep(0.4)

    if geteuid in content:
        print("Patching...")
        content = content.replace(geteuid, getppid)
        with open(target, "wb") as binary:
            binary.write(content)

        sleep(0.8)
        print("Done.")
        #print("\n", content)

    elif getppid in content:
        print(f"[ERROR]: File '{bn}' already seems to be patched. Aborting.")

    else:
        print("[ERROR]: Sorry, an unexpected error occurred. Aborting.")

except FileNotFoundError:
    print(f"[ERROR]: File '{target}' not found.")
